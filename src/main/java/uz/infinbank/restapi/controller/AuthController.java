package uz.infinbank.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.infinbank.restapi.dto.RequestDto;
import uz.infinbank.restapi.dto.ResponseDto;
import uz.infinbank.restapi.service.PerimetrService;
import uz.infinbank.restapi.service.SurfaceAre;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private PerimetrService perimetr;
    @Autowired
    private SurfaceAre are;

    @PostMapping("/perimetr")
    public ResponseEntity<ResponseDto> getPerimetr(@RequestBody  RequestDto requestDto){
        return perimetr.perimetr(requestDto);
    }

    @PostMapping("/area")
    public ResponseEntity<ResponseDto> getArea(@RequestBody  RequestDto requestDto){
        return are.surface(requestDto);
    }
}
