package uz.infinbank.restapi.service;

import org.springframework.http.ResponseEntity;
import uz.infinbank.restapi.dto.RequestDto;
import uz.infinbank.restapi.dto.ResponseDto;

public interface PerimetrService {
    ResponseEntity<ResponseDto> perimetr(RequestDto dto);

    ResponseDto perimetrCircle(double[] doubles);
    ResponseDto perimetrTriangle(double[] doubles);
    ResponseDto perimetrRectangle(double[] doubles);
    ResponseDto perimetrSquare(double[] doubles);
}
