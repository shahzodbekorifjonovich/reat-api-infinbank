package uz.infinbank.restapi.service;

import org.springframework.http.ResponseEntity;
import uz.infinbank.restapi.dto.RequestDto;
import uz.infinbank.restapi.dto.ResponseDto;

public interface SurfaceAre {
    ResponseEntity<ResponseDto> surface(RequestDto dto);
    ResponseDto surfaceRound(double[] doubles);
    ResponseDto surfaceTriangle(double[] doubles);
    ResponseDto surfaceRectangle(double[] doubles);
    ResponseDto surfaceSquare(double[] doubles);

}
