package uz.infinbank.restapi.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.infinbank.restapi.dto.RequestDto;
import uz.infinbank.restapi.dto.ResponseDto;

@Service
public class SurfaceAreaImpl implements SurfaceAre {
    @Override
    public ResponseEntity<ResponseDto> surface(RequestDto dto) {
        ResponseDto response = new ResponseDto();
        double[] array = dto.getSide();
        if (dto.getName() != null && array.length != 0) {

            if (dto.getName().equalsIgnoreCase("round")) {
                response = surfaceRound(array);
            } else if (dto.getName().equalsIgnoreCase("triangle")) {
                response = surfaceTriangle(array);
            } else if (dto.getName().equalsIgnoreCase("rectangle")) {
                response = surfaceRectangle(array);
            } else if (dto.getName().equalsIgnoreCase("square")) {
                response = surfaceSquare(array);
            }
            else {
                response.setData(null);
                response.setMessage("Wrong name  ! Example name => Round,trianGle,Rectangle,square");
                response.setSucces(false);
            }
        } else {
            response.setData(null);
            response.setMessage("Name or Side is empty");
            response.setSucces(false);
        }

        return ResponseEntity.accepted().body(response);
    }

    @Override
    public ResponseDto surfaceRound(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 1) {
            response.setData(Math.PI * Math.pow(doubles[0], 2));
            response.setSucces(true);
            response.setMessage("Done round area");
        } else {
            response.setData(null);
            response.setMessage("wrong round side!");
            response.setSucces(false);
        }
        return response;
    }

    @Override
    public ResponseDto surfaceTriangle(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 3) {
            double p = (doubles[0] + doubles[1] + doubles[2]) / 2;
            double P = Math.pow(p * (p - doubles[0]) * (p - doubles[1]) * (p - doubles[2]), 1 / 2);
            response.setData(P);
            response.setSucces(true);
            response.setMessage("Done Triangle area");
        } else {
            response.setData(null);
            response.setMessage("Wrong triangle side");
            response.setSucces(false);
        }
        return response;
    }

    @Override
    public ResponseDto surfaceRectangle(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 2) {
            response.setData(doubles[0] * doubles[1]);
            response.setSucces(true);
            response.setMessage("Done rectangle area");
        } else {
            response.setData(null);
            response.setMessage("Rectangle side wrong");
            response.setSucces(false);
        }
        return response;
    }

    @Override
    public ResponseDto surfaceSquare(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 1) {
            response.setData(Math.pow(doubles[0], 2));
            response.setSucces(true);
            response.setMessage("Done square area");
        } else {
            response.setData(null);
            response.setMessage("rectangle side is wrong!");
            response.setSucces(false);
        }
        return response;
    }
}
