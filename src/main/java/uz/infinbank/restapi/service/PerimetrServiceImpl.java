package uz.infinbank.restapi.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.infinbank.restapi.dto.RequestDto;
import uz.infinbank.restapi.dto.ResponseDto;

@Service
public class PerimetrServiceImpl implements PerimetrService {
    @Override
    public ResponseEntity<ResponseDto> perimetr(RequestDto dto) {
        ResponseDto response = new ResponseDto();
        double[] array = dto.getSide();
        if (dto.getName() != null && array.length != 0) {

            if (dto.getName().equalsIgnoreCase("circle")) {
                response = perimetrCircle(array);
            } else if (dto.getName().equalsIgnoreCase("triangle")) {
                response = perimetrTriangle(array);
            } else if (dto.getName().equalsIgnoreCase("rectangle")) {
                response = perimetrRectangle(array);
            } else if (dto.getName().equalsIgnoreCase("square")) {
                response = perimetrSquare(array);
            } else {
                response.setData(null);
                response.setMessage("Wrong name  ! Example name => Circle,trianGle,Rectangle,square");
                response.setSucces(false);
            }
        } else {
            response.setData(null);
            response.setMessage("Name or Side is empty");
            response.setSucces(false);
        }
        return ResponseEntity.accepted().body(response);
    }

    @Override
    public ResponseDto perimetrCircle(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 1) {
            response.setData(2 * Math.PI * doubles[0]);
            response.setSucces(true);
            response.setMessage("Done Circle Perimetr");
        } else {
            response.setData(null);
            response.setMessage("Circle side wrong ! , side length is one");
            response.setSucces(false);
        }
        return response;
    }

    @Override
    public ResponseDto perimetrTriangle(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 3) {
            response.setData(doubles[0] + doubles[1] + doubles[2]);
            response.setSucces(true);
            response.setMessage("Done triangle Perimetr");
        } else {
            response.setData(null);
            response.setMessage("Triangle side wrong ! side length is 3 ");
            response.setSucces(false);
        }
        return response;
    }

    @Override
    public ResponseDto perimetrRectangle(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 2) {
            response.setData(2 * (doubles[0] + doubles[1]));
            response.setSucces(true);
            response.setMessage("Done Rectangle Perimetr");
        } else {
            response.setData(null);
            response.setMessage("Rectangle side wrong ! side length is 4 ");
            response.setSucces(false);
        }
        return response;
    }

    @Override
    public ResponseDto perimetrSquare(double[] doubles) {
        ResponseDto response = new ResponseDto();
        if (doubles.length == 1) {
            response.setData(4 * doubles[0]);
            response.setSucces(true);
            response.setMessage("Done Square perimetr");
        } else {
            response.setData(null);
            response.setMessage("Square side wrong ! side length is 1 ");
            response.setSucces(false);
        }
        return response;
    }
}
