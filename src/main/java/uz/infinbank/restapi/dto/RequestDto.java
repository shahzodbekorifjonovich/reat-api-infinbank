package uz.infinbank.restapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestDto {
    @NotNull
    private String name;
    @Size(min = 1 ,max = 3)
    private double[] side;

}
